"""Класс конвертера из Римского числа в Арабское."""


class Solution:
    def romanToInt(self, s: str) -> int:
        """Перевод из Римской СС в Арабскую

        Args:
            s (str): Римское число

        Returns:
            int: Арабское число
        """
        numbers = {'I': 1, 'V': 5, 'X': 10,
                   'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        result = 0
        length = len(s)
        for i in range(length-1):
            if numbers[s[i]] < numbers[s[i+1]]:
                result -= numbers[s[i]]
            elif numbers[s[i]] >= numbers[s[i+1]]:
                result += numbers[s[i]]

        result += numbers[s[len(s)-1]]
        return result


roman_parse = Solution()
answer = roman_parse.romanToInt('I')
print(answer)
